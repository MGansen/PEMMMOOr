% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/vectorizedHVapproaches.R
\name{RHV_per_Basin}
\alias{RHV_per_Basin}
\title{Relative Hypervolume per Basin
Contrasts the HV of a reference set and the HV reached by a Solutionset separately for each basin}
\usage{
RHV_per_Basin(RefSet, SolutionSet, BasinInformation, RefPoint = NULL)
}
\description{
Relative Hypervolume per Basin
Contrasts the HV of a reference set and the HV reached by a Solutionset separately for each basin
}
