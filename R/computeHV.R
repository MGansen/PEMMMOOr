##Calling the HV functions from C
##using logic and code from ecr2 by Jakob Bossek
#TODO: Protection against false inputs
#'computeHV
compute_HV = function(data, ref_point = NULL){

  if(is.null(ref_point)){ref_point = getNadir(data)}

  data = t(data)

  return(.Call("computeHVC", as.matrix(data), as.numeric(ref_point)))

}
