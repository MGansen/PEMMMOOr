## IGD XY Matching Variants

# comparing exact number might run into scaling issues or become impossible with IGDP.
# --> use Rank based comparison
#data for ref and solution set as one dataframe, x1,...xn and y1,...,yn as collums, points as rows
#IGDP_Version 0 for IGD, else see IGDP
#Combination_version: how should the distances from parameter and objective space be combined? 0 = mean rank, 1= mean normalized distance (min_max normalization)
# in case of ties, they can be resolved by only using the dist in x or y

#'@export
IGD_XY = function (Ref_Set = NULL, Solution_Set = NULL, dim_x, dim_y, p = 1, IGDP_Version = 0, combination_version = 0, solve_ties = "y"){

  print("Alternative, deprecated version. Instead of selecting the closest point in parameter space a combined calculation is done over both spaces.")

  ##generate base data: distances Ref_Set to Solution_Set in parameter space and objective space
  X_ref = Ref_Set[,1:dim_x] %>%
    as.matrix()
  Y_ref = Ref_Set[,(dim_x + 1) : (dim_x + dim_y)]%>%
    as.matrix()
  X_sol = Solution_Set[,1:dim_x] %>%
    as.matrix()
  Y_sol = Solution_Set[,(dim_x + 1) : (dim_x + dim_y)]%>%
    as.matrix()
  distances_X = dist_matrix_sol_ref(X_ref, X_sol, dim_x, version = 0)
  distances_Y = dist_matrix_sol_ref(Y_ref, Y_sol, dim_y, version = IGDP_Version)

  #combine distance information
  if (combination_version == 0){

    norm_X = t(apply(distances_X, 1, rank))
    norm_Y = t(apply(distances_Y, 1, rank))


  } else if (combination_version == 1){

    norm_X = t(apply(distances_X, 1, min_max_normalization))
    norm_Y = t(apply(distances_Y, 1, min_max_normalization))

  }

  norm_XY = (norm_X + norm_Y) / 2

  #get distances to only the "overall closest neighbor"
  filter = (norm_XY == apply(norm_XY, 1, min))

  ##catch scenarios with ties
  if (sum(filter) > nrow(filter)){

    for (i in 1:nrow(filter)){

      if (sum(filter[i,]) != 1){

        if(solve_ties == "y"){

          norm_XY[i,] = (-1) * (filter[i,] * norm_Y[i,])# minimal values are replaced with distances (negativ distances used to go below the 0s produced by the boolean vector)

        }else if(solve_ties == "x"){

          norm_XY[i,] = (-1) * (filter[i,] * norm_X[i,])

        }

      }

    }

    filter = (norm_XY == apply(norm_XY, 1, min))

  }



  closest_dist =filter *distances_Y

  distances = apply(closest_dist, 1, sum)

 # print(distances)

  #compute IGD(P)
  res = compute_gd(distances, p)

  return(res)

}



#'IGDXY
#'@description: Calculates the average distance in objective space for the closest points in parameter space
#'
#'@param RefSet: A reference set over parameter and objective space (x1,...xn,y1,....ym), points as rows
#'@param Solution_Set: A solution set over parameter and objective space (x1,...xn,y1,....ym), points as rows
#'@param dim_x: dimension of the parameter space
#'@param dim_y: dimension of the objective space
#'@param p: IGD parameter, see IGD/GD function
#'@param IGD_Version: Which distance version should be used in the objective space? 0 (IGD), 1/2 (IGDP), 3/4 (MMIGDP)
#'@export
IGDXY = function (Ref_Set = NULL, Solution_Set = NULL, dim_x, dim_y, p = 1, IGD_Version = 0){

  ##generate base data: distances Ref_Set to Solution_Set in parameter space and objective space
  X_ref = Ref_Set[,1:dim_x] %>%
    as.matrix()
  Y_ref = Ref_Set[,(dim_x + 1) : (dim_x + dim_y)]%>%
    as.matrix()
  X_sol = Solution_Set[,1:dim_x] %>%
    as.matrix()
  Y_sol = Solution_Set[,(dim_x + 1) : (dim_x + dim_y)]%>%
    as.matrix()

  distances = c()

  for ( i in 1:nrow(Ref_Set)){

    smallest = smallest_dist_point_to_set(as.matrix(X_sol), X_ref[i,], dim_x, 0)
    smallest = smallest[length(smallest) - 1] + 1

    distances[i] = smallest_dist_point_to_set(as.matrix(Y_sol), Y_ref[i,], dim_y, IGD_Version)[smallest]

  }


  #compute IGD(P)
  res = compute_gd(distances, p)

  return(res)

}
