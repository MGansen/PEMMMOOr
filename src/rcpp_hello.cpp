#include <Rcpp.h>
using namespace Rcpp;

// This is a simple function using Rcpp that creates an R list
// containing a character vector and a numeric vector.
//
// Learn more about how to use Rcpp at:
//
//   http://www.rcpp.org/
//   http://adv-r.had.co.nz/Rcpp.html
//
// and browse examples of code using Rcpp at:
//
//   http://gallery.rcpp.org/
//


double eucledian_norm_cpp (NumericVector x, NumericVector y, int dimension){

  double res = 0;

  for (int i = 0; i < dimension; i++){

    res += pow((x[i] - y[i]),2);

  }

  return sqrt(res);

}

double Ishibuchi_eucledian_norm_cpp (NumericVector x, NumericVector y, int dimension){

  double dist[dimension];
  double res = 0;

  for (int i = 0; i < dimension; i++){

    double d = (x[i] - y[i]);

    if (d < 0){
      d = 0;
    }

    res += pow(d, 2);

  }

  return sqrt(res);

}

double Ishibuchi_MM_eucledian_norm_cpp (NumericVector x, NumericVector y, int dimension){

  double dist[dimension];
  double res = 0;

  for (int i = 0; i < dimension; i++){

    double d = (x[i] - y[i]);

    if (d < 0){
      d = 0;
    }

    res += pow(d, 2);

  }

  res = sqrt(res);

  if(res == 0){
    if (!(eucledian_norm_cpp(x,y, dimension) == 0)){
      res = 1.79769e+308;
    }
  }

  return (res);

}


// [[Rcpp::export]]
double eucledian_norm (NumericVector x, NumericVector y, int dimension){

  double res = 0;

  for (int i = 0; i < dimension; i++){

    res += pow((x[i] - y[i]),2);

  }

  return sqrt(res);

}



// [[Rcpp::export]]
NumericMatrix eucledian_dist_matrix (NumericMatrix data, int dimension){

  int n = data.rows();

  Rcpp:NumericMatrix res(n,n);

  for (int i = 0; i < n; i++){
    for (int j = i; j < n; j++){
      Rcpp::NumericVector x(dimension);
      Rcpp::NumericVector y(dimension);
      for(int k = 0; k<dimension; k++){
        x[k] = data(i,k);
        y[k] = data(j,k);
      }
      double r_dist = eucledian_norm_cpp(x,y, dimension);
      res(i,j) = r_dist;
      res(j,i) = r_dist;
    }
  }

  return (res);

}


// [[Rcpp::export]]
NumericVector dist_point_to_set (NumericMatrix set, NumericVector point, int dimension){

  int n = set.rows();

  Rcpp::NumericVector res(n);

  for (int i = 0; i < n; i++){

    Rcpp::NumericVector y(dimension);

    for(int k = 0; k<dimension; k++){
      y[k] = set(i,k);
    }


    double dist = eucledian_norm_cpp(point, y, dimension);
    res[i] = dist;

  }

  return (res);

}



// returns vector of all distances with the indice of the closest point in second to last place
// and the distance to the clocest point in the last place of the vector
// int version refers to distance options
//versions: 0 = eucledian, 1 = Ishibuchi (Minimization), 2 = Ishibuchi (Maximization)
// 3 = Multi-Modal Ishibuchi (Minimization), 4 = Multi-Modal Ishibuchi (Maximization)
// [[Rcpp::export]]
NumericVector smallest_dist_point_to_set (NumericMatrix set, NumericVector point, int dimension, int version){

  int n = set.rows();

  Rcpp::NumericVector res((n + 2));
  double dist;

  for (int i = 0; i < n; i++){

    Rcpp::NumericVector y (dimension);

    for(int k = 0; k<dimension; k++){
      y[k] = set(i,k);
    }

    if (version == 0){
      dist = eucledian_norm_cpp(y, point, dimension);
    }
    if (version == 1){
      dist = Ishibuchi_eucledian_norm_cpp(y, point, dimension);
    }
    if (version == 2){
      dist = Ishibuchi_eucledian_norm_cpp(point, y, dimension);
    }
    if (version == 3){
      dist = Ishibuchi_MM_eucledian_norm_cpp(y, point, dimension);
    }
    if (version == 4){
      dist = Ishibuchi_MM_eucledian_norm_cpp(point, y, dimension);
    }



    res[i] = dist;

    if (i == 0) {
      res[(n+1)] = dist;
      res[n]= i;
    }

    if(dist < res[(n+1)]){
      res[(n+1)] = dist;
      res[n]= i;
    }



  }

  return (res);

}

// [[Rcpp::export]]
int one_nn_rcpp (NumericVector train_x1, NumericVector train_x2, NumericVector labels, double point_x1, double point_x2){

  int size_train = train_x1.length();

  double dist[size_train];

  int smallest_i;

  for (int i = 0; i < size_train; i++){

    dist[i] = sqrt(pow((train_x1[i] - point_x1),2) + pow((train_x2[i] - point_x2),2));

    if(i == 0){ smallest_i = i;}
    if(dist[i] < dist[smallest_i]){smallest_i = i;}

  }

  return labels[smallest_i];

}

// [[Rcpp::export]]
LogicalVector non_dominated_set_point(NumericMatrix data, NumericVector point) {

  int dimension = point.length();
  int n = data.rows();
  LogicalVector res(n);

  for (int i = 0; i<n ; ++i){
    bool dom = false;
    bool eq = true;

    for (int j = 0; j < dimension; ++j){
      dom = dom || (data(i,j) < point[j]);
      eq = eq &&  (data(i,j) == point[j]);
    }

    res[i] =dom || eq;

  }

  return res;

}

// [[Rcpp::export]]
bool dominating_point_set(NumericVector point, NumericMatrix data) {

  int dimension = point.length();
  int n = data.rows();
  bool res = true;

  for (int i = 0; i<n ; ++i){
    bool dom = true;
    bool strictly = false;
    bool eq = true;

    for (int j = 0; j < dimension; ++j){
      strictly = strictly || (data(i,j) > point[j]);
      dom = dom && (data(i,j) >= point[j]);
      eq = eq &&  (data(i,j) == point[j]);
    }


    res = res && (dom && strictly);
  }



  return res;

}

// [[Rcpp::export]]
bool dominating_point_set_any(NumericVector point, NumericMatrix data) {

  int dimension = point.length();
  int n = data.rows();
  bool res = false;

  for (int i = 0; i<n ; ++i){
    bool dom = true;
    bool strictly = false;
    bool eq = true;

    for (int j = 0; j < dimension; ++j){
      strictly = strictly || (data(i,j) > point[j]);
      dom = dom && (data(i,j) >= point[j]);
      eq = eq &&  (data(i,j) == point[j]);
    }


    res = res || (dom && strictly);
  }



  return res;

}

//not strictly, to not remove equal points
// [[Rcpp::export]]
bool is_dominated_point_set_any(NumericVector point, NumericMatrix data) {

  int dimension = point.length();
  int n = data.rows();
  bool res = false;

  for (int i = 0; i<n ; ++i){
    bool dom = true;
    bool strictly = false;


    for (int j = 0; j < dimension; ++j){
      strictly = strictly || (data(i,j) < point[j]);
      dom = dom && (data(i,j) <= point[j]);
    }


    res = res || dom; // (dom && strictly);
  }



  return res;

}



// [[Rcpp::export]]
LogicalVector non_dominated_set(NumericMatrix data){

  int dimension = data.cols();
  int n = data.rows();
  LogicalVector res(n, true);

  for (int i = 0; i < n; ++i){

    NumericVector point = data(i , _);

    res = res & non_dominated_set_point(data, point);


  }


  return(res);



}
